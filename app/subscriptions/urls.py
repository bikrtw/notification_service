from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import ClientViewSet, SubscriptionViewSet, MessageViewSet

router = DefaultRouter()
router.register('client', ClientViewSet)
router.register('subscription', SubscriptionViewSet)
router.register(r'subscription/(?P<sub_id>\d+)/messages', MessageViewSet,
                basename='subscription-messages')

urlpatterns = [
    path('v1/', include(router.urls)),
]
