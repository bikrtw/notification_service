from rest_framework import mixins, viewsets
from rest_framework.generics import get_object_or_404

from .models import Client, Subscription, Message
from .serializers import ClientSerializer, SubscriptionSerializer, \
    MessageSerializer


class ClientViewSet(mixins.CreateModelMixin, mixins.UpdateModelMixin,
                    mixins.DestroyModelMixin, viewsets.GenericViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class SubscriptionViewSet(mixins.CreateModelMixin, mixins.UpdateModelMixin,
                          mixins.DestroyModelMixin, mixins.ListModelMixin,
                          viewsets.GenericViewSet):
    queryset = Subscription.objects.all()
    serializer_class = SubscriptionSerializer


class MessageViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    serializer_class = MessageSerializer

    def get_queryset(self):
        subscription_id = self.kwargs.get('sub_id')
        subscription = get_object_or_404(Subscription, pk=subscription_id)
        return Message.objects.filter(subscription=subscription)
