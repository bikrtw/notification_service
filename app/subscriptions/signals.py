from django.db.models.signals import post_save
from django.dispatch import receiver

from . import tasks
from .models import Client, Message, Subscription


@receiver(post_save, sender=Subscription)
def perform_create(sender, instance, **kwargs):
    """ Create tasks and message objects on Subscription creation."""

    # all clients with same tag or same mobile operator code
    clients = (
        Client.objects.filter(tag=instance.tag)
        | Client.objects.filter(
            mobile_operator_code=instance.mobile_operator_code))

    messages = [Message(subscription=instance, client=client)
                for client in clients]
    Message.objects.bulk_create(messages)

    for message in messages:
        tasks.api_send_message.send(message.id)
