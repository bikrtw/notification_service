from enum import Enum

import pytz
from django.core.validators import RegexValidator
from django.db import models
from django.utils import timezone

TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))


class SendStatus(Enum):
    SUCCESS = 'success'
    PENDING = 'pending'
    ERROR = 'error'

    @classmethod
    def choices(cls):
        """Return choices suitable for django"""
        return tuple((i.name, i.value) for i in cls)


class Subscription(models.Model):
    date_start = models.DateTimeField(default=timezone.now)
    date_finish = models.DateTimeField()
    text = models.TextField()
    mobile_operator_code = models.CharField(
        max_length=32, null=True, blank=True)
    tag = models.CharField(max_length=128, blank=True)

    def __str__(self):
        return (f'Subscription from {self.date_start} to {self.date_finish}'
                f'with text "{self.text}"')


class Client(models.Model):
    phone_regex = RegexValidator(
        regex=r'^7\d{10}$',
        message='Phone number must be entered in the format: "7XXXXXXXXXX", '
                'where X means any digit'
    )
    phone_number = models.CharField(validators=[phone_regex], max_length=11)
    mobile_operator_code = models.CharField(max_length=32)
    tag = models.CharField(max_length=128)
    timezone = models.CharField(max_length=64, choices=TIMEZONES,
                                default='UTC')

    def __str__(self):
        return f'Client with phone number {self.phone_number}'


class Message(models.Model):
    creation_date = models.DateTimeField(auto_now_add=True)
    send_status = models.CharField(
        choices=SendStatus.choices(),
        default=SendStatus.PENDING,
        max_length=64,
    )
    subscription = models.ForeignKey(
        Subscription,
        on_delete=models.CASCADE,
        related_name='messages',
    )
    client = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,
        related_name='messages',
    )
