from django.contrib import admin

from .models import Subscription, Client, Message

admin.site.register(Message)
admin.site.register(Client)
admin.site.register(Subscription)
