# Generated by Django 4.0.3 on 2022-03-18 04:22

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('subscriptions', '0002_alter_message_send_status_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subscription',
            name='date_start',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
