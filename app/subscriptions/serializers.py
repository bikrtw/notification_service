from typing import List

from django.db.models import Count
from rest_framework import serializers

from .models import Client, Subscription, Message


class ClientSerializer(serializers.ModelSerializer):

    class Meta:
        model = Client
        fields = ('id', 'phone_number', 'mobile_operator_code', 'tag',
                  'timezone')


class SubscriptionSerializer(serializers.ModelSerializer):
    messages = serializers.SerializerMethodField()

    class Meta:
        model = Subscription
        fields = ('id', 'date_start', 'date_finish', 'text',
                  'mobile_operator_code', 'tag', 'messages')

    def get_messages(self, obj: Subscription) -> List[dict]:
        messages = (Message.objects.filter(subscription=obj)
                    .values('send_status')
                    .order_by('send_status')
                    .annotate(count=Count('send_status')))
        return list(messages)


class MessageSerializer(serializers.ModelSerializer):
    creation_date = serializers.DateTimeField()

    class Meta:
        model = Message
        fields = ('id', 'creation_date', 'send_status',
                  'client', 'subscription')
