import os
from typing import Tuple

import dramatiq
import requests
from django.utils import timezone

from .models import Message, SendStatus


@dramatiq.actor
def api_send_message(message_id: int) -> None:
    """Send message using external API.

    MESSAGE_API_URL ENV should be set with other API related values.
    """
    try:
        message = Message.objects.select_related(
            'client', 'subscription').get(pk=message_id)
    except Message.DoesNotExist:
        print(f'Unable to find message with id {message_id} in db.')
        return

    current_datetime = timezone.now()

    # missed our chance to send message
    if message.subscription.date_finish < current_datetime:
        message.send_status = SendStatus.ERROR
        message.save()
        print(f'Message with id {message.id} was NOT sent '
              'due to subscription time expiration')
        return

    # subscription is not started - will postpone
    if message.subscription.date_start > current_datetime:
        delay = (message.subscription.date_start - current_datetime
                 ).total_seconds() * 1000  # convert ms to s
        # recreate send task with delay
        api_send_message.send_with_options(args=(message.id,), delay=delay)
        print(f'Message with id {message.id} will be sent in {delay} seconds')
        return

    send_message(message)
    message.send_status = SendStatus.SUCCESS
    message.save()
    print(f'message with id={message.pk} was sent')


def send_message(message: Message) -> None:
    """Actually perform API request to send message."""
    url, data, headers = construct_request(message)
    response = requests.post(url=url, json=data, headers=headers)

    # API request failed
    if not response.ok:
        raise ConnectionError(
            f'Unable to send message with id {message.id}, '
            f'API responded with status {response.status_code}')


def construct_request(message: Message) -> Tuple[str, dict, dict]:
    """Construct API request to send message.

    :returns url, POST body, request headers"""

    message_api_url = os.getenv('MESSAGE_API_URL') + str(message.pk)
    api_token = os.getenv('MESSAGE_API_TOKEN')
    headers = {'Authorization': f'Bearer {api_token}'}
    data = {
        'id': message.pk,
        'phone': int(message.client.phone_number),
        'text': message.subscription.text,
    }

    return message_api_url, data, headers
